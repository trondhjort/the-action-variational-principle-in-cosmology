# The Action Variational Principle in Cosmology

Thesis submitted for the degree of Candidatus Scientiarum at the University of Oslo in June 1999.

## Abstract
During the last decade, the action variational principle of Peebles has proven to
be a very applicable tool in the study of the formation of large-scale structures in
the immediate neighbourhood of the MilkyWay. It provides us with the mean to
recreate the orbits of the galaxies from epochs early in the history of the Universe
until the present, which in turn opens for a closer study of a number of subjects
concerning the evolution of the Universe. This study will give a thorough presentation
of this variational principle and its areas of application, with an emphasis
on the numerical aspect. In practise, the use of the action variational principle is
a question of numerical optimization, a side of the method which previously has
not been considered in detail. The major part of this study is therefore devoted
to the testing of several dierent optimizing methods, attempting to locate the
most eÆcient one. Trials have indicated that the performance of the methods
varies considerably with certain properties of the system of mass tracers; e.g.
size. The general consensus from numerical literature is that optimizing methods
using the second-derivative of the action works best for small systems, while for
large systems, the methods using only the rst-derivative are preferable. The
results here indicate that a combination of these two types of methods work best
when having the parameterization of the orbits. In addition to these tests, the action
variational principle has also been applied to a system consisting of 22 mass
tracers in the Local Group and its immediate neighbourhood, where a couple of
newly discovered galaxies has been included. The distances to some of the more
distant mass tracers have been adjusted to give a better t between predicted
and observed radial velocities.

**Web:** [http://trond.hjorteland.com/thesis/hoved.html](http://trond.hjorteland.com/thesis/hoved.html)