!     Subroutine for calculating the coefficients C_{i,n} from a given
!     startvalue using the Steepest Decent and Newton-Raphson method.
!     ----------------------------------------------------------------
!
SUBROUTINE calc
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : action,hessian,gradient,&
       lnsrch,ludcmp,lubksb,secant,choldc,cholsl
  IMPLICIT NONE
  INTEGER :: i
  INTEGER, DIMENSION(dim) :: indx
  REAL :: A1,oe,lmda,act,actold,stpmax,normgradold=100
  REAL, PARAMETER :: tol=0.005
  REAL, DIMENSION(dim) :: dir,diag,c
  REAL, DIMENSION(dim,dim) :: mat  
  LOGICAL :: check
  ALLOCATE (hess(dim,dim))
!
! Initializing some variables:
!
  stpmax = MAX(SQRT(DOT_PRODUCT(coeff,coeff)),REAL(dim))
  act = action(coeff)
!
! Starting the iteration:
!
  DO
!
! Calculating the gradient:
!
     grad = gradient(coeff)
     normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
! Output:
!
!!$     A1 = MODULO(it,10)
!!$     IF (A1 == 0) THEN
!!$        PRINT '(22x,I7,7x,E12.4)',it,normgrad
!!$     END IF
!
! Termination test:
!
     IF (normgrad < eps) EXIT
     it = it + 1
!
! Calculating, using the Steepest decent for normgrad > stopsteep
! and Newton Raphson for normgrad <= stopsteep.
!
     IF (ABS(normgradold-normgrad) > tol*(1.0_dp+normgrad)) THEN
        dir = -grad 
     ELSE
        dir = -grad
        hess = hessian(coeff)
        mat = hess
        CALL choldc(mat,diag)
        IF (ALL(diag==0)) THEN           ! i.e. non-positive definite   
           CALL ludcmp(hess,indx,oe)
           CALL lubksb(hess,indx,dir)
        ELSE
           CALL cholsl(mat,diag,dir,dir)
        END IF
     END IF
     coeff = secant(coeff,dir,lmda)
     normgradold = normgrad
!
  END DO
!
  hess = hessian(coeff)
  DEALLOCATE(grad)
!
END SUBROUTINE calc



































































































