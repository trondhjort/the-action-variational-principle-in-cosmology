!     Subroutine for calculating the coefficients C_{i,n} from a given 
!     startvalue using the Conjugate gradient method.
!     -----------------------------------------------------------------
!
SUBROUTINE calc
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : gradient,hessian,secant
  IMPLICIT NONE
  REAL :: dgg,lmda,normgradold,gam,gg,tmp,tol=0.01
  REAL, DIMENSION(dim) :: gi,di,dir
!
! Initializing:
!
  grad = gradient(coeff)
  gi = -grad
  di = gi
  dir = di
  normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
! Start the iterations:
!
  DO
     it = it + 1
!
!    Calculating the steplength, coeffecients and gradient:
!
     coeff = secant(coeff,dir,lmda)
     grad = gradient(coeff)
     normgradold = normgrad
     normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
!    Output:
!
!!$     tmp = MODULO(it,100)
!!$     IF (tmp == 0) THEN
!!$        PRINT '(22x,I7,7x,E12.4)',it,normgrad
!!$     END IF
!
!    Termination test:
!
     IF (normgrad < eps) EXIT
!
!    Calculate the new direction:
! 
     IF (ABS(DOT_PRODUCT(grad,gi)) > tol*normgradold**2) THEN
        gi = -grad
        di = gi
        dir = di
     ELSE
        gg = DOT_PRODUCT(gi,gi)
!        dgg = DOT_PRODUCT(grad,grad)      ! Fletcher-Reeves
        dgg = DOT_PRODUCT(grad+gi,grad)   ! Polak-Ribiere
        IF (gg == 0.0) EXIT
        gam = dgg/gg
        gi = -grad
        di = gi + gam*di
        dir = di
     END IF
!
  END DO
!
  DEALLOCATE(grad)
  ALLOCATE (hess(dim,dim))
  hess = hessian(coeff)
!
END SUBROUTINE calc



