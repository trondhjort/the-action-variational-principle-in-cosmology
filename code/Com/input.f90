!     Subroutine for reading parameters and data from file
!     ----------------------------------------------------
!
SUBROUTINE input
!
  USE nrtype; USE avp_data
  IMPLICIT NONE
  INTEGER :: i,s,tst
  REAL :: mass_M31 
  ALLOCATE (opt(2))
!
! Reading inputfile:
!
  OPEN (UNIT=1,STATUS='OLD',FILE='Com/input.dta')
  READ(1,*) bodies
  READ(1,*) nodes,N_trial,eps,startvalue
  READ(1,*) h,omega,cut,mass_M31
  READ(1,*) redshift,pts
  READ(1,*) opt(1),opt(2)
  CLOSE (UNIT=1)
!
! Setting the dimension and the allocating some arrays:
!
  dim = bodies*3*N_trial
  ALLOCATE (coeff(dim))
  ALLOCATE (dec(bodies))
  ALLOCATE (ra(bodies))
  ALLOCATE (d(bodies))
  ALLOCATE (mass(bodies))
  ALLOCATE (x_0(3,bodies))
!
! Reading galaxydata from file:
!
  OPEN (UNIT=2,STATUS='OLD',FILE='Com/galaxies.dta')
  DO i = 1,bodies
     READ(2,*) mass(i),ra(i),dec(i),d(i)
  END DO
  CLOSE (UNIT=2)
!
! Transformation from celestial to Cartesian coordinates:
!
  x_0(1,:) = d(:)*COSD(dec(:))*COSD(ra(:))
  x_0(2,:) = d(:)*COSD(dec(:))*SIND(ra(:))
  x_0(3,:) = d(:)*SIND(dec(:))
!
! Setting masses relative to M31:
!
  mass = mass*mass_M31
!
! Initializing the coefficients, either an initial guess, or 
! reading old values from file.
!
IF (opt(1) == 0) THEN
     coeff = startvalue
  ELSE
     startvalue = 1E12
     OPEN (UNIT=3,STATUS='OLD',FILE='coeff_in.dta')
     READ(3,*) tst
     IF (tst /= dim) THEN
        PRINT '(/,10X,"Halted: wrong dimension of inputfile coeff_in.dta",/)'
        STOP
     END IF
     DO s = 1,dim
        READ(3,*) coeff(s)
     END DO
     CLOSE (UNIT=3)
  END IF
!
END SUBROUTINE input
