!     Subroutine for writing data to file.
!     ------------------------------------
!
SUBROUTINE output
!
  USE nrtype; USE avp_data   
  USE avp_intf, ONLY : tred2,tqli
  IMPLICIT NONE
  INTEGER :: k,i,s,t
  REAL, DIMENSION(dim) :: diag,e
!
  ALLOCATE (r(3,bodies))
!
! Write results:
!
  PRINT '(22x,I7,7x,E12.4,E12.4)',it,normgrad
!
  PRINT '(///,17x,"I      RADIAL VELOCITY      C_X, n=1",/)'
  s = 1
  DO i = 1,bodies 
     PRINT '(16x,I2,10x,F7.2,8x,F10.5)',i,rdot_rad(i),coeff(s)
     s = s + 3*N_trial
  END DO
!
! Determine the type of solution:
!
  CALL tred2(hess,diag,e,.TRUE.)
  CALL tqli(diag,e)
  PRINT '(//)'
  PRINT '(25x,"COND. NUM.:",F12.2)',MAXVAL(ABS(diag))/MINVAL(ABS(diag))
  IF (ALL(diag >= 0)) THEN 
     PRINT '(22x,"SOLUTION IS A MINIMUM POINT")'
  ELSEIF (ALL(diag < 0)) THEN
     PRINT '(22x,"SOLUTION IS A MAXIMUM POINT")'
  ELSE
     PRINT '(22x,"SOLUTION IS A SADDLE POINT")'
  END IF
!
  PRINT '(//)'
  PRINT '(16x,"CPU-TIME SPENT ON CALCULATION:",F10.2,//)',dt
!
! Write coeffecients to file:
!
  OPEN (UNIT=4,STATUS='REPLACE',FILE='coeff_out.dta')
  WRITE (4,*) dim
  DO s = 1,dim
     WRITE(4,*) coeff(s)
  END DO
  CLOSE (UNIT=4) 
!
! Write orbits to file:
!
  OPEN (UNIT=5,STATUS='REPLACE',FILE='orbits.dta')
  DO t = 1,pts
     DO i = 1,bodies
        WRITE (5,*) (x(k,i,t),k=1,3)
     END DO
  END DO
  CLOSE (UNIT=5) 
!
! Transforming initial positions to physical coordinates:
!
  DO i = 1,bodies
     r(:,i) = x(:,i,1)*a(1)
  END DO
!
! Write the initial values (at z) to file:
!
  OPEN (UNIT=6,STATUS='REPLACE',FILE='init.dta')
  DO i = 1,bodies
     WRITE(6,*) mass(i),(r(k,i),k=1,3),(rdot(k,i,1),k=1,3)
  END DO
  CLOSE (UNIT=6)
!
! Write the radial velocities to file:
!
  OPEN (UNIT=7,STATUS='REPLACE',FILE='rad_vel.dta')
  DO i = 2,bodies
     WRITE(7,*) rdot_rad(i)
  END DO
  CLOSE (UNIT=7)
!
END SUBROUTINE output



