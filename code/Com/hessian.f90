!     Subroutine for the determination of the Hessian matrix.
!     -------------------------------------------------------
!
FUNCTION hessian(c)
!
  USE nrtype; USE avp_data
  USE avp_func, ONLY : pos
  IMPLICIT NONE
  REAL, DIMENSION(:), INTENT(IN) :: c 
  REAL, DIMENSION(size(c),size(c)) :: hessian 
  INTEGER :: alpha,beta,col,i,j,k,m,n,row,t
  REAL :: A1,T1,T2,T3,dxabs
  REAL, DIMENSION(4) :: dx
  REAL, DIMENSION(3,bodies,nodes) :: xi
  REAL, DIMENSION(3,3) :: tens
!
  dx(4) = cutoff
!
! Calculate the position:
!
  xi = pos(c,nodes)
!
! Calculating the terms of the Jacobi matrix, integrating using the 
! Gauss Quadrature, W(x) = s^(1/2):
!
  hessian = 0.0
  DO t = 1,nodes
     DO i = 1,bodies
        DO j = 1,bodies
           tens = 0.0 
           IF (i /= j) THEN
              dx(1:3) = xi(:,i,t) - xi(:,j,t)
              dxabs = SQRT(DOT_PRODUCT(dx,dx))
              DO alpha = 1,3
                 DO beta = 1,3
                    IF (alpha == beta) THEN
                       A1 = mass(j)*(3.0_dp*dx(alpha)*dx(beta)/dxabs**5 - &
                            1.0_dp/dxabs**3)
                       tens(beta,alpha) = -A1
                    ELSE
                       A1 = mass(j)*(3.0_dp*dx(alpha)*dx(beta)/dxabs**5)
                       tens(beta,alpha) = -A1
                    END IF
                 END DO
              END DO
           ELSE
              DO k = 1,bodies
                 IF (k /= i) THEN
                    dx(1:3) = xi(:,i,t) - xi(:,k,t)
                    dxabs = SQRT(DOT_PRODUCT(dx,dx))
                    DO alpha = 1,3
                       DO beta = 1,3
                          IF (alpha == beta) THEN
                             A1 = mass(k)*(3.0_dp*dx(alpha)*dx(beta)/dxabs**5 - &
                                  1.0_dp/dxabs**3)
                             tens(beta,alpha) = tens(beta,alpha) + A1
                          ELSE
                             A1 = mass(k)*(3.0_dp*dx(alpha)*dx(beta)/dxabs**5)
                             tens(beta,alpha) = tens(beta,alpha) + A1
                          END IF
                       END DO
                    END DO
                 END IF
              END DO
           END IF
           DO alpha = 1,3
              DO beta = 1,3
                 DO m = 1,N_trial
                    DO n = 1,N_trial
                       col = (j-1)*3*N_trial + (beta-1)*N_trial + m
                       row = (i-1)*3*N_trial + (alpha-1)*N_trial + n
                       T1 = 0.0; T2 = 0.0
                       IF (alpha == beta .AND. i == j) THEN
                          T1 = dtrial(m,t)*dtrial(n,t)*a(t)**2
                          T2 = trial(m,t)*trial(n,t)*Omega*H_0**2/(2.*a(t))
                       END IF
                       T3 = trial(m,t)*trial(n,t)*tens(beta,alpha)*G/a(t)
                       hessian(row,col) = hessian(row,col) + & 
                            mass(i)*weight(t)*SQRT(a(t))*(T1+T2+T3)/adot(t)
                    END DO
                 END DO
              END DO
           END DO
        END DO
     END DO
  END DO
!
END FUNCTION hessian
