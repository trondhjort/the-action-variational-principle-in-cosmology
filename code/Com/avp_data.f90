!     Module for declaration of global parameters and variables
!     ---------------------------------------------------------
!
MODULE avp_data
!
  USE nrtype
!
  IMPLICIT NONE
!
! Parameters:
!
  REAL, PARAMETER                      :: G=4.49293E-3_dp
                                          ! Mpc, Gyr & 1.E12 solar masses.
!
! Variables:
!
  REAL                                 :: H_0
  REAL                                 :: M_T      
  INTEGER                              :: N_trial
  REAL                                 :: R_0
  REAL, DIMENSION(:), ALLOCATABLE      :: a
  REAL, DIMENSION(:), ALLOCATABLE      :: adot
  INTEGER                              :: bodies
  REAL                                 :: cut
  REAL                                 :: cutoff
  REAL, DIMENSION(:), ALLOCATABLE      :: coeff
  REAL, DIMENSION(:), ALLOCATABLE      :: d
  REAL, DIMENSION(:), ALLOCATABLE      :: dec
  INTEGER                              :: dim
  REAL                                 :: dt
  REAL, DIMENSION(:,:), ALLOCATABLE    :: dtrial
  REAL                                 :: eps
  LOGICAL                              :: finished
  REAL, DIMENSION(:), ALLOCATABLE      :: grad
  REAL                                 :: h
  REAL, DIMENSION(:,:), ALLOCATABLE    :: hess
  REAL, DIMENSION(:,:), ALLOCATABLE    :: hessin
  INTEGER                              :: it
  REAL, DIMENSION(:), ALLOCATABLE      :: mass
  INTEGER                              :: nodes
  REAL                                 :: normgrad
  REAL                                 :: omega
  INTEGER, DIMENSION(:), ALLOCATABLE   :: opt
  INTEGER                              :: pts
  REAL, DIMENSION(:,:), ALLOCATABLE    :: r
  REAL, DIMENSION(:), ALLOCATABLE      :: ra
  REAL, DIMENSION(:,:,:), ALLOCATABLE  :: rdot
  REAL, DIMENSION(:), ALLOCATABLE      :: rdot_rad
  REAL                                 :: redshift
  REAL                                 :: rho_0
  REAL*4                               :: startvalue
  REAL, DIMENSION(:,:), ALLOCATABLE    :: trial
  REAL, DIMENSION(:), ALLOCATABLE      :: weight
  REAL, DIMENSION(:,:), ALLOCATABLE    :: x_0
  REAL, DIMENSION(:,:), ALLOCATABLE    :: x_0_cm
  REAL, DIMENSION(:,:,:), ALLOCATABLE  :: x
!
END MODULE avp_data
