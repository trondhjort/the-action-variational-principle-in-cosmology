!     Subroutine for calculating the trial functions etc.
!     ---------------------------------------------------
!
SUBROUTINE trialfunc(steps)
!
  USE nrtype; USE avp_data
  USE avp_func, ONLY : fac
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: steps
  INTEGER :: n,t,s
  REAL :: F,bin,nfac,sfac,snfac
!
  IF (finished) THEN
     DEALLOCATE (adot,trial,dtrial)
     ALLOCATE (adot(steps))
     ALLOCATE (trial(N_trial,steps))
     ALLOCATE (dtrial(N_trial,steps))
  END IF
!
  adot = 0.0; trial = 0.0; dtrial = 0.0
!
! Calculating da/dt (adot):
!
  DO t = 1,steps
     F = Omega + (1.0_dp-Omega)*a(t)**3
     adot(t) = H_0*SQRT(F/a(t))
  END DO
!
! Trial functions of the form a^n*(1-a):
!
  IF (opt(2) == 0) THEN
     DO t = 1,steps
        DO n = 0,(N_trial-1)
           trial(n+1,t) = (1.0_dp - a(t))*a(t)**n
           dtrial(n+1,t) = (n*(1.0_dp - a(t))*a(t)**(n-1) - a(t)**n)*adot(t)
        END DO
     END DO
  END IF
!
! Trial functions on the Bernoulli-form:
!
  IF (opt(2) == 1) THEN
     sfac = fac(N_trial)
     DO n = 0,(N_trial-1)
        nfac = fac(n)
        snfac = fac(N_trial-n)
        bin = sfac/(nfac*snfac)
        DO t = 1,steps
           trial(n+1,t) = (1.0_dp - a(t))**(N_trial-n)*a(t)**n*bin
           IF (a(t) == 1.0) THEN
              IF (n == N_trial-1) THEN
                 dtrial(n+1,t) = -bin*adot(t)
              ELSE
                 dtrial(n+1,t) = 0.0
              END IF
           ELSE
              dtrial(n+1,t)=((1.0_dp-a(t))**(N_trial-n)*a(t)**(n-1)*n - &
                   (1.0_dp-a(t))**(N_trial-n-1)*a(t)**n*(N_trial-n))*bin*adot(t)
           END IF
        END DO
     END DO
  END IF
!
END SUBROUTINE trialfunc
