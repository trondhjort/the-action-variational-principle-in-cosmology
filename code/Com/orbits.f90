!     Subroutine for calculating the orbits.
!     --------------------------------------
!
SUBROUTINE orbits
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : trialfunc
  USE avp_func, ONLY : pos
  IMPLICIT NONE
  INTEGER :: i,k,t,s1,s2
  REAL :: B1,B2,B3,B4,C1,C2,C3,C4,ai,axdot
!
  DEALLOCATE (a)
  ALLOCATE (a(pts))
  ALLOCATE (rdot(3,bodies,2))
  ALLOCATE (rdot_rad(bodies))
  ALLOCATE (x(3,bodies,pts))
!
! Determine the expansion coeffecient at `pts' number of times from 
! z to today:
!
  ai = 1.0_dp/(1.0_dp + redshift)
  DO t = 0,(pts-1)
     a(t+1)= ai + (1.0_dp-ai)*t/(pts-1)
  END DO
!
! Calculating the trial functions, its derivatives, and da/dt (adot):
!
  CALL trialfunc(pts)
!
! Calculating the orbits and the physical velocities at a=1 and a=1+z:
!
  x = pos(coeff,pts)
  DO i = 1,bodies
     DO k = 1,3
        s1 = (i-1)*3*N_trial + (k-1)*N_trial + 1
        s2 = s1 + (N_trial-1)
        axdot = a(1)*SUM(coeff(s1:s2)*dtrial(:,1))
        rdot(k,i,1) = axdot + x(k,i,1)*adot(1)
        axdot = a(pts)*SUM(coeff(s1:s2)*dtrial(:,pts))
        rdot(k,i,2) = axdot + x(k,i,pts)*adot(pts)
     END DO  
  END DO

!
! Calculating the current radial velocity, relative to the MW:
!
  rdot_rad(1) = 0.0
  DO i = 2,bodies
     rdot_rad(i) = DOT_PRODUCT(rdot(:,i,2)-rdot(:,1,2),x_0(:,i))/d(i)
  END DO
  rdot_rad = rdot_rad*3.0856E19_dp/3.1536E16_dp   ! Mpc -> km, Gyr -> s
!
! Rotating the coordinate system so that the x-axis is pointing along the
! line connecting MW and M31:
!     Rotating into the xz-plane:
!
 DO i = 1,bodies
     DO t = 1,pts
        B1 = x(1,i,t)*COSD(ra(2))  + x(2,i,t)*SIND(ra(2))
        B2 = x(1,i,t)*-SIND(ra(2)) + x(2,i,t)*COSD(ra(2))
        x(1,i,t) = B1
        x(2,i,t) = B2
     END DO
     B3 = rdot(1,i,1)*COSD(ra(2))  + rdot(2,i,1)*SIND(ra(2))
     B4 = rdot(1,i,1)*-SIND(ra(2)) + rdot(2,i,1)*COSD(ra(2))
     rdot(1,i,1) = B3
     rdot(2,i,1) = B4
  END DO
!
!     Rotating into the xy-plane:
!
  DO i = 1,bodies
     DO t = 1,pts
        C1 = x(1,i,t)*COSD(dec(2))  + x(3,i,t)*SIND(dec(2))
        C2 = x(1,i,t)*-SIND(dec(2)) + x(3,i,t)*COSD(dec(2))
        x(1,i,t) = C1
        x(3,i,t) = C2
     END DO
     C3 = rdot(1,i,1)*COSD(dec(2))  + rdot(3,i,1)*SIND(dec(2))
     C4 = rdot(1,i,1)*-SIND(dec(2)) + rdot(3,i,1)*COSD(dec(2))
     rdot(1,i,1) = C3
     rdot(3,i,1) = C4 
  END DO
!
END SUBROUTINE orbits
