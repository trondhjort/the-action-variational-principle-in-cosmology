!     Function for calculating the gradient of the action.
!     ----------------------------------------------------
!
FUNCTION gradient(co)
!
  USE nrtype; USE avp_data
  USE avp_func, ONLY : pos
  IMPLICIT NONE
  REAL, DIMENSION(:), INTENT(IN) :: co 
  REAL, DIMENSION(size(co)) :: gradient
  INTEGER :: i,j,k,n,s,s1,s2,t
  REAL :: T1,T2,T3,xdot,dxabs
  REAL, DIMENSION(3) :: grav
  REAL, DIMENSION(4)  :: dx
  REAL, DIMENSION(3,bodies,nodes) :: xi
!
  dx(4) = cutoff
!
! Calculate the position:
!
  xi = pos(co,nodes)
!
! Calculating the gradient, and integrate using the Gaussian Qudrature, 
! W(x) = a^(1/2):
!
  gradient = 0.0 
  DO t = 1,nodes
     DO i = 1,bodies
        grav = 0.0
        DO j = 1,bodies
           IF (j /= i) THEN
              dx(1:3) = xi(:,j,t) - xi(:,i,t)
              dxabs = SQRT(DOT_PRODUCT(dx,dx))
              grav(1:3) = grav(1:3) + dx(1:3)*mass(j)/dxabs**3
           END IF
        END DO
        DO k = 1,3
           s1 = (i-1)*3*N_trial + (k-1)*N_trial + 1
           s2 = s1 + (N_trial-1)
           xdot = SUM(co(s1:s2)*dtrial(:,t))
           DO n = 1,N_trial
              s = (i-1)*3*N_trial + (k-1)*N_trial + n
              T1 = xdot*dtrial(n,t)*a(t)**2
              T2 = trial(n,t)*xi(k,i,t)*Omega*H_0**2/(2.0_dp*a(t))
              T3 = trial(n,t)*grav(k)*G/a(t)
              gradient(s) = gradient(s) + &
                   mass(i)*weight(t)*SQRT(a(t))*(T1+T2+T3)/adot(t)
           END DO
        END DO
     END DO
  END DO
!
END FUNCTION gradient
