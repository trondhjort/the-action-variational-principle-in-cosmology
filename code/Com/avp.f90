!       A C T I O N  V A R I A T I O N A L  P R I N C I P L E
!       *****************************************************
!
!                Code by Trond Hjorteland, ITA, Oslo.
!                       (except where noted) 
!                ------------------------------------
!
PROGRAM avp
!
  USE nrtype; USE avp_data 
  USE avp_intf
  IMPLICIT NONE
  REAL :: t1,t2 
!
! Reading data from file:
!
  CALL input
!
! Initializing parameters/data:
!
  CALL initial
!
! Calculating the coefficients:
!
  CALL CPU_TIME(t1)
!
  CALL calc
!
  CALL CPU_TIME(t2)
  dt = ABS(t2 - t1)
!
! Calculating the orbits:
!
  CALL orbits
!
! Writing data to file:
!
  CALL output
!
END PROGRAM avp

