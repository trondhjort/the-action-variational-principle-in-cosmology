!     Function for linear search using the secant method.
!     ---------------------------------------------------
!
FUNCTION secant(c,d,eps)
!
  USE nrtype; USE avp_func, ONLY : dfunc,func
  IMPLICIT NONE
  REAL, DIMENSION(:), INTENT(IN) :: c,d
  REAL, INTENT(OUT) :: eps
  REAL, DIMENSION(size(c)) :: secant,cnew
  INTEGER ::iter
  INTEGER, PARAMETER :: itmax=20
  REAL :: gd,gtaud,tau,deps,w,z
  REAL, PARAMETER :: tol=1.0E-4_dp
!
  eps = 0.0; tau = 0.5
!
  cnew = c
  DO iter = 1,itmax
     deps = 0.0
     gd = dfunc(deps,cnew,d)
     gtaud = dfunc(tau,cnew,d)
     deps = -tau*gd/(gtaud-gd)
     eps = eps + deps  
     cnew(:) = cnew(:) + deps*d(:)
     IF (ABS(deps) < TOL) EXIT
     tau = -deps
  END DO
  secant = cnew 
!
END FUNCTION secant
