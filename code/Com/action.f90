!     Function for calculating the value of the action
!     ------------------------------------------------
!
FUNCTION action(c)
!
  USE nrtype; USE avp_data
  USE avp_func, ONLY : pos
  IMPLICIT NONE
  REAL :: action
  REAL, DIMENSION(:), INTENT(IN) :: c 
  INTEGER :: i,j,k,s1,s2,t
  REAL :: kin,pot,pot_corr,potj,xdot2,dxabs
  REAL, DIMENSION(4) :: dx
  REAL, DIMENSION(3,bodies,nodes) :: xi
!
  dx(4) = cutoff
!
! Calculate the positions:
!
  xi = pos(c,nodes)
!
! Calculate the action, integrating using the Gaussian quadrature method,
! W(x) = a^(1/2):
!
  action = 0.0
  DO t = 1,nodes
     kin = 0.0; pot = 0.0; pot_corr = 0.0
     DO i = 1,bodies
        IF (i <= bodies-1) THEN
           potj = 0.0
           DO j = i+1,bodies
              dx(1:3) = xi(:,i,t) - xi(:,j,t)
              dxabs = SQRT(DOT_PRODUCT(dx,dx)) 
              potj = potj + mass(j)/dxabs
           END DO
           pot = pot  + G*mass(i)*potj/a(t)
        END IF
        xdot2  = 0.0
        DO k = 1,3
           s1 = (i-1)*3*N_trial + (k-1)*N_trial + 1
           s2 = s1 + (N_trial-1)
           xdot2 = xdot2 + (SUM(c(s1:s2)*dtrial(:,t)))**2
        END DO
        kin = kin + mass(i)*xdot2*a(t)**2/2.0_dp
        pot_corr = pot_corr + &
             mass(i)*DOT_PRODUCT(xi(:,i,t),xi(:,i,t))*Omega*H_0**2/(4.0_dp*a(t))  
     END DO
     action = action + weight(t)*SQRT(a(t))*(kin+pot+pot_corr)/adot(t)
  END DO
!
END FUNCTION action
