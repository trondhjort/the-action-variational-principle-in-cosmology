!     Subroutine for initialization.
!     ------------------------------
!
SUBROUTINE initial
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : gauleg,gradient,trialfunc
  IMPLICIT NONE
  INTEGER :: i,k
  REAL, DIMENSION(3) :: cmx
  REAL, DIMENSION(nodes*2) :: xi,wi
!
! Allocating arrays:
!
  ALLOCATE (a(nodes))
  ALLOCATE (adot(nodes))
  ALLOCATE (dtrial(N_trial,nodes))
  ALLOCATE (grad(dim))
  ALLOCATE (trial(N_trial,nodes))
  ALLOCATE (weight(nodes))
  ALLOCATE (x_0_cm(3,bodies))
!
! Initializing some parameters:
!
  H_0 = h*0.1022_dp    !given in 1/Gyr.
  finished = .FALSE.
!
! Form total mass and centre of mass displacements.
!
  M_T = SUM(mass)
!
  cmx = 0.0
  DO i = 1,bodies
     cmx(:) = cmx(:) + mass(i)*x_0(:,i)
  END DO
!
! Adjust coordinates and velocities to c.m. rest frame:
!
  DO i = 1,bodies
     x_0_cm(:,i) = x_0(:,i) - cmx(:)/M_T
  END DO
!
! Calculate the radius of the sphere containing all the masses, R_0,
! and the cutoff:
!
  R_0 = (2.0_dp*M_T*G/(omega*H_0**2))**(1.0_dp/3.0_dp)
  cutoff = cut*R_0/100.0_dp
!
! Print data to screen:
!
  PRINT '(///,22x,"ACTION VARIATIONAL PRINCIPLE")'
  PRINT '(22X,"****************************",/)'
  PRINT '(/,8X,"BODIES     EPS     START-COEFF   h    OMEGA    PTS    N ",/)'
  PRINT '(8x,I4,4x,ES8.1,5x,F5.2,5x,F5.2,3x,F3.1,3x,I5,3x,I2)',&
       bodies,eps,startvalue,h,omega,pts,N_trial
  PRINT '(//,10X,"I        MASS         X_CM         Y_CM         Z_CM",/)'
  DO i = 1,bodies
     PRINT '(8x,I3,F12.2,3F13.2)',i,mass(i),(x_0_cm(k,i),k=1,3)
  END DO
  PRINT '(//,24X,"CUTOFF   R_0   TOTAL MASS",/)'
  PRINT '(24x,F5.2,3x,F5.2,4x,F5.2)',cutoff,R_0,M_T
!
! Calculating the Gaussian quadrature abscissas and weights, using
! weightfunction W(a) = a^(-1/2):
!
  CALL gauleg(-1.0,1.0,xi,wi)
!
  a(:) = xi(nodes+1:2*nodes)**2
  weight(:) = wi(nodes+1:2*nodes)*2.0_dp
!
  PRINT '(//,22X," ITERATIONS         EPS",/)'
!
! Calculating the trial function, the derivate trial function, and 
! dt/da (adot) for each node and n:
!
  CALL trialfunc(nodes)
  finished = .TRUE.
!
END SUBROUTINE initial
