MODULE avp_intf
!
  INTERFACE
     FUNCTION action(c)
       REAL, DIMENSION(:), INTENT(IN) :: c
       REAL :: action
     END FUNCTION action
  END INTERFACE
!
  INTERFACE
     SUBROUTINE calc
     END SUBROUTINE calc
  END INTERFACE
!
  INTERFACE
     SUBROUTINE choldc(a,p)
       REAL, DIMENSION(:,:), INTENT(INOUT) :: a
       REAL, DIMENSION(:), INTENT(OUT) :: p
     END SUBROUTINE choldc
  END INTERFACE
!
  INTERFACE
     SUBROUTINE cholsl(a,p,b,x)
       REAL, DIMENSION(:,:), INTENT(IN) :: a
       REAL, DIMENSION(:), INTENT(IN) :: p,b
       REAL, DIMENSION(:), INTENT(INOUT) :: x
     END SUBROUTINE cholsl
  END INTERFACE
!
  INTERFACE
     FUNCTION davidon(c,d,eps)
       REAL, DIMENSION(:) :: c,d
       REAL, INTENT(OUT) :: eps
       REAL, DIMENSION(size(c)) :: davidon
     END FUNCTION davidon
  END INTERFACE
!
  INTERFACE
     SUBROUTINE gauleg(x1,x2,x,w)
       REAL, INTENT(IN) :: x1,x2
       REAL, DIMENSION(:), INTENT(OUT) :: x,w
     END SUBROUTINE gauleg
  END INTERFACE
!
  INTERFACE
     FUNCTION gradient2(c)
       REAL, DIMENSION(:), INTENT(IN) :: c
       REAL, DIMENSION(size(c)) :: gradient2
     END FUNCTION gradient2
  END INTERFACE
!
  INTERFACE
     FUNCTION gradient(c)
       REAL, DIMENSION(:), INTENT(IN) :: c
       REAL, DIMENSION(size(c)) :: gradient
     END FUNCTION gradient
  END INTERFACE 
!
  INTERFACE
     FUNCTION hessian(c)
       REAL, DIMENSION(:), INTENT(IN) :: c 
       REAL, DIMENSION(size(c),size(c)) :: hessian
     END FUNCTION hessian
  END INTERFACE
!
  INTERFACE
     SUBROUTINE input
     END SUBROUTINE input
  END INTERFACE
!
  INTERFACE      
     SUBROUTINE initial
     END SUBROUTINE initial
  END INTERFACE
!
  INTERFACE 
     SUBROUTINE lnsrch(xold,fold,g,p,x,f,stpmax,check,func)
       REAL, DIMENSION(:), INTENT(IN) :: xold,g
       REAL, DIMENSION(:), INTENT(INOUT) :: p
       REAL, INTENT(IN) :: fold,stpmax
       REAL, DIMENSION(:), INTENT(OUT) :: x
       REAL, INTENT(OUT) :: f
       LOGICAL, INTENT(OUT) :: check
       INTERFACE
          FUNCTION func(x)
            USE nrtype
            IMPLICIT NONE
            REAL(SP) :: func
            REAL(SP), DIMENSION(:), INTENT(IN) :: x
          END FUNCTION func
       END INTERFACE
     END SUBROUTINE lnsrch
  END INTERFACE
!
  INTERFACE
     SUBROUTINE lubksb(a,indx,b)
       USE nrtype
       IMPLICIT NONE
       REAL(SP), DIMENSION(:,:), INTENT(IN) :: a
       INTEGER(I4B), DIMENSION(:), INTENT(IN) :: indx
       REAL(SP), DIMENSION(:), INTENT(INOUT) :: b
     END SUBROUTINE lubksb
  END INTERFACE
!
  INTERFACE
     SUBROUTINE ludcmp(a,indx,d)
       USE nrtype
       IMPLICIT NONE
       REAL, DIMENSION(:,:), INTENT(INOUT) :: a
       INTEGER, DIMENSION(:), INTENT(OUT) :: indx
       REAL, INTENT(OUT) :: d
     END SUBROUTINE ludcmp
  END INTERFACE
!
  INTERFACE
     SUBROUTINE orbits
     END SUBROUTINE orbits
  END INTERFACE
!
  INTERFACE
     SUBROUTINE output
     END SUBROUTINE output
  END INTERFACE
!
  INTERFACE 
     FUNCTION pythag(a,b)
       REAL, INTENT(IN) :: a,b
       REAL :: pythag
     END FUNCTION pythag
  END INTERFACE
!
  INTERFACE
     FUNCTION secant(c,d,eps)
       REAL, DIMENSION(:) :: c,d
       REAL, INTENT(OUT) :: eps
       REAL, DIMENSION(size(c)) :: secant
     END FUNCTION secant
  END INTERFACE
!
  INTERFACE 
     SUBROUTINE tqli(d,e,z)
       REAL, DIMENSION(:), INTENT(INOUT) :: d,e
       REAL, DIMENSION(:,:), OPTIONAL, INTENT(INOUT) :: z
     END SUBROUTINE tqli
  END INTERFACE
!
  INTERFACE 
     SUBROUTINE tred2(a,d,e,novectors)
       REAL, DIMENSION(:,:), INTENT(INOUT) :: a
       REAL, DIMENSION(:), INTENT(OUT) :: d,e
       LOGICAL, OPTIONAL, INTENT(IN) :: novectors
     END SUBROUTINE tred2
  END INTERFACE
!
  INTERFACE
     SUBROUTINE trialfunc(steps)
       INTEGER, INTENT (IN) :: steps
     END SUBROUTINE trialfunc
  END INTERFACE
!
  INTERFACE
     SUBROUTINE posdef(a,pos)
       REAL, DIMENSION(:,:), INTENT(IN) :: a
       LOGICAL, INTENT(OUT)  :: pos
     END SUBROUTINE posdef
  END INTERFACE
!
  INTERFACE
     SUBROUTINE rotate(r,i,a,b)
       REAL, DIMENSION(:,:), TARGET, INTENT(INOUT) :: r
       INTEGER, INTENT(IN) :: i
       REAL, INTENT(IN) :: a,b
     END SUBROUTINE rotate
  END INTERFACE
!
  INTERFACE
     SUBROUTINE qrupdt(r,u,v)
       REAL, DIMENSION(:,:), INTENT(INOUT) :: r
       REAL, DIMENSION(:), INTENT(INOUT) :: u
       REAL, DIMENSION(:), INTENT(IN) :: v
     END SUBROUTINE qrupdt
  END INTERFACE
!
END MODULE avp_intf
