MODULE  avp_func
!
  USE nrtype
!
CONTAINS
!
  FUNCTION dfunc(lmda,c_old,d)
    USE avp_intf, ONLY : gradient
    IMPLICIT NONE
    REAL :: dfunc
    REAL, INTENT(In) :: lmda
    REAL, DIMENSION(:), INTENT(In) :: c_old,d
    REAL, DIMENSION(SIZE(C_old)) :: df,c_new
    c_new(:) = c_old(:) + lmda*d(:)
    df = gradient(c_new)
    dfunc = DOT_PRODUCT(df,d)
  END FUNCTION dfunc
!
  FUNCTION fac(base)
    IMPLICIT NONE
    REAL :: fac
    INTEGER, INTENT(IN) :: base
    INTEGER :: s
    fac = 1.0
    DO s = 1,base
       fac = fac*s
    END DO
  END FUNCTION fac
!
  FUNCTION func(lmda,c_old,d)
    USE avp_intf, ONLY : action
    IMPLICIT NONE
    REAL :: func
    REAL, INTENT(IN) :: lmda
    REAL, DIMENSION(:), INTENT(IN) :: c_old,d
    REAL, DIMENSION(SIZE(c_old)) :: c_new
    c_new(:) = c_old(:) + lmda*d(:)
    func = action(c_new)
  END FUNCTION func
!
  FUNCTION pos(c,steps)
    USE avp_data 
    IMPLICIT NONE
    REAL, DIMENSION(:), INTENT(IN) :: c
    INTEGER,  INTENT(IN) :: steps
    REAL, DIMENSION(3,bodies,steps) :: pos
    INTEGER :: i,k,t,s1,s2
    pos = 0.0
    DO t = 1,steps
       DO i = 1,bodies
          DO k = 1,3
             s1 = (i-1)*3*N_trial + (k-1)*N_trial + 1
             s2 = s1 + N_trial-1
             pos(k,i,t) = x_0_cm(k,i) + SUM(c(s1:s2)*trial(:,t))
          END DO
       END DO
    END DO
  END FUNCTION pos
!
END MODULE avp_func

