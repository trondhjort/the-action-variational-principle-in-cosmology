SUBROUTINE lnsrch(xold,fold,g,p,x,f,stpmax,check,func)
  USE nrtype; USE nrutil, ONLY : assert_eq,nrerror,vabs
  IMPLICIT NONE
  REAL(SP), DIMENSION(:), INTENT(IN) :: xold,g
  REAL(SP), DIMENSION(:), INTENT(INOUT) :: p
  REAL(SP), INTENT(IN) :: fold,stpmax
  REAL(SP), DIMENSION(:), INTENT(OUT) :: x
  REAL(SP), INTENT(OUT) :: f
  LOGICAL(LGT), INTENT(OUT) :: check
  INTERFACE
     FUNCTION func(x)
       USE nrtype
       IMPLICIT NONE
       REAL(SP) :: func
       REAL(SP), DIMENSION(:), INTENT(IN) :: x
     END FUNCTION func
  END INTERFACE
  REAL(SP), PARAMETER :: ALF=1.0e-4_sp,TOLX=EPSILON(x)
  INTEGER(I4B) :: ndum
  REAL(SP) :: a,alam,alam2,alamin,b,disc,f2,fold2,pabs,rhs1,rhs2,slope,&
       tmplam
  ndum=assert_eq(SIZE(g),SIZE(p),SIZE(x),SIZE(xold),'lnsrch')
  check=.FALSE.
  pabs=vabs(p(:))
  IF (pabs > stpmax) p(:)=p(:)*stpmax/pabs
  slope=DOT_PRODUCT(g,p)
  alamin=TOLX/MAXVAL(ABS(p(:))/MAX(ABS(xold(:)),1.0_sp))
  alam=1.0
  DO
     x(:)=xold(:)+alam*p(:)
     f=func(x)
     IF (alam < alamin) THEN
        x(:)=xold(:)+p(:)
        check=.TRUE.
        RETURN
     ELSE IF (f <= fold+ALF*alam*slope) THEN
        RETURN
     ELSE
        IF (alam == 1.0) THEN
           tmplam=-slope/(2.0_sp*(f-fold-slope))
        ELSE
           rhs1=f-fold-alam*slope
           rhs2=f2-fold2-alam2*slope
           a=(rhs1/alam**2-rhs2/alam2**2)/(alam-alam2)
           b=(-alam2*rhs1/alam**2+alam*rhs2/alam2**2)/&
                (alam-alam2)
           IF (a == 0.0) THEN
              tmplam=-slope/(2.0_sp*b)
           ELSE
              disc=b*b-3.0_sp*a*slope
              !IF (disc < 0.0) CALL nrerror('roundoff problem in lnsrch')
              IF (disc < 0.0) THEN
                 x(:)=xold(:)+p(:)
                 EXIT
              END IF
              tmplam=(-b+SQRT(disc))/(3.0_sp*a)
           END IF
           IF (tmplam > 0.5_sp*alam) tmplam=0.5_sp*alam
        END IF
     END IF
     alam2=alam
     f2=f
     fold2=fold
     alam=MAX(tmplam,0.1_sp*alam)
  END DO
END SUBROUTINE lnsrch
