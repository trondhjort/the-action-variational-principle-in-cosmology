SUBROUTINE rotate(r,i,a,b)
  USE nrtype; USE nrutil, ONLY : assert_eq
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), TARGET, INTENT(INOUT) :: r
  INTEGER(I4B), INTENT(IN) :: i
  REAL(SP), INTENT(IN) :: a,b
  REAL(SP), DIMENSION(SIZE(r,1)) :: temp
  INTEGER(I4B) :: n
  REAL(SP) :: c,fact,s
  n=assert_eq(SIZE(r,1),SIZE(r,2),'rotate')
  IF (a == 0.0) THEN
     c=0.0
     s=SIGN(1.0_sp,b)
  ELSE IF (ABS(a) > ABS(b)) THEN
     fact=b/a
     c=SIGN(1.0_sp/SQRT(1.0_sp+fact**2),a)
     s=fact*c
  ELSE
     fact=a/b
     s=SIGN(1.0_sp/SQRT(1.0_sp+fact**2),b)
     c=fact*s
  END IF
  temp(i:n)=r(i,i:n)
  r(i,i:n)=c*temp(i:n)-s*r(i+1,i:n)
  r(i+1,i:n)=s*temp(i:n)+c*r(i+1,i:n)
END SUBROUTINE rotate
