FUNCTION pythag(a,b)
  USE nrtype
  IMPLICIT NONE
  REAL(DP), INTENT(IN) :: a,b
  REAL(DP) :: pythag
  REAL(DP) :: absa,absb
  absa=ABS(a)
  absb=ABS(b)
  IF (absa > absb) THEN
     pythag=absa*SQRT(1.0_dp+(absb/absa)**2)
  ELSE
     IF (absb == 0.0) THEN
        pythag=0.0
     ELSE
        pythag=absb*SQRT(1.0_dp+(absa/absb)**2)
     END IF
  END IF
END FUNCTION pythag
