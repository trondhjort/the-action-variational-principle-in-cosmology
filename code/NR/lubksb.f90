SUBROUTINE lubksb(a,indx,b)
  USE nrtype; USE nrutil, ONLY : assert_eq
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), INTENT(IN) :: a
  INTEGER(I4B), DIMENSION(:), INTENT(IN) :: indx
  REAL(SP), DIMENSION(:), INTENT(INOUT) :: b
  INTEGER(I4B) :: i,n,ii,ll
  REAL(SP) :: summ
  n=assert_eq(SIZE(a,1),SIZE(a,2),SIZE(indx),'lubksb')
  ii=0
  DO i=1,n
     ll=indx(i)
     summ=b(ll)
     b(ll)=b(i)
     IF (ii /= 0) THEN
        summ=summ-DOT_PRODUCT(a(i,ii:i-1),b(ii:i-1))
     ELSE IF (summ /= 0.0) THEN
        ii=i
     END IF
     b(i)=summ
  END DO
  DO i=n,1,-1
     b(i) = (b(i)-DOT_PRODUCT(a(i,i+1:n),b(i+1:n)))/a(i,i)
  END DO
END SUBROUTINE lubksb
