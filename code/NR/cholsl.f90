SUBROUTINE cholsl(a,p,b,x)
  USE nrtype; USE nrutil, ONLY : assert_eq
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), INTENT(IN) :: a
  REAL(SP), DIMENSION(:), INTENT(IN) :: p,b
  REAL(SP), DIMENSION(:), INTENT(INOUT) :: x
  INTEGER(I4B) :: i,n
  n=assert_eq((/SIZE(a,1),SIZE(a,2),SIZE(p),SIZE(b),SIZE(x)/),'cholsl')
  DO i=1,n
     x(i)=(b(i)-DOT_PRODUCT(a(i,1:i-1),x(1:i-1)))/p(i)
  END DO
  DO i=n,1,-1
     x(i)=(x(i)-DOT_PRODUCT(a(i+1:n,i),x(i+1:n)))/p(i)
  END DO
END SUBROUTINE cholsl
