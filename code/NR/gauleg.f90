SUBROUTINE gauleg(x1,x2,x,w)
  USE nrtype; USE nrutil, ONLY : arth,assert_eq,nrerror
  IMPLICIT NONE
  REAL(DP), INTENT(IN) :: x1,x2
  REAL(DP), DIMENSION(:), INTENT(OUT) :: x,w
  REAL(DP), PARAMETER :: EPS=1.0e-14_dp
  INTEGER(I4B) :: its,j,m,n
  INTEGER(I4B), PARAMETER :: MAXIT=10
  REAL(DP) :: xl,xm
  REAL(DP), DIMENSION((SIZE(x)+1)/2) :: p1,p2,p3,pp,z,z1
  LOGICAL(LGT), DIMENSION((SIZE(x)+1)/2) :: unfinished
  n=assert_eq(SIZE(x),SIZE(w),'gauleg')
  m=(n+1)/2
  xm=0.5_dp*(x2+x1)
  xl=0.5_dp*(x2-x1)
  z=COS(PI_D*(arth(1,1,m)-0.25_dp)/(n+0.5_dp))
  unfinished=.TRUE.
  DO its=1,MAXIT
     WHERE (unfinished)
        p1=1.0
        p2=0.0
     END WHERE
     DO j=1,n
        WHERE (unfinished)
           p3=p2
           p2=p1
           p1=((2.0_dp*j-1.0_dp)*z*p2-(j-1.0_dp)*p3)/j
        END WHERE
     END DO
     WHERE (unfinished)
        pp=n*(z*p1-p2)/(z*z-1.0_dp)
        z1=z
        z=z1-p1/pp
        unfinished=(ABS(z-z1) > EPS)
     END WHERE
     IF (.NOT. ANY(unfinished)) EXIT
  END DO
  IF (its == MAXIT+1) CALL nrerror('too many iterations in gauleg')
  x(1:m)=xm-xl*z
  x(n:n-m+1:-1)=xm+xl*z
  w(1:m)=2.0_dp*xl/((1.0_dp-z**2)*pp**2)
  w(n:n-m+1:-1)=w(1:m)
END SUBROUTINE gauleg

