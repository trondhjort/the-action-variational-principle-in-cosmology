SUBROUTINE qrupdt(r,u,v)
  USE nrtype; USE nrutil, ONLY : assert_eq,ifirstloc
  USE avp_intf, ONLY : rotate,pythag
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: r
  REAL(SP), DIMENSION(:), INTENT(INOUT) :: u
  REAL(SP), DIMENSION(:), INTENT(IN) :: v
  INTEGER(I4B) :: i,k,n
  n=assert_eq((/SIZE(r,1),SIZE(r,2),SIZE(u),&
       SIZE(v)/),'qrupdt')
  k=n+1-ifirstloc(u(n:1:-1) /= 0.0)
  IF (k < 1) k=1
  DO i=k-1,1,-1
     CALL rotate(r,i,u(i),-u(i+1))
     u(i)=pythag(u(i),u(i+1))
  END DO
  r(1,:)=r(1,:)+u(1)*v
  DO i=1,k-1
     CALL rotate(r,i,r(i,i),-r(i+1,i))
  END DO
END SUBROUTINE qrupdt
