SUBROUTINE choldc(a,p)
  USE nrtype; USE nrutil, ONLY : assert_eq,nrerror
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: a
  REAL(SP), DIMENSION(:), INTENT(OUT) :: p
  INTEGER(I4B) :: i,n
  REAL(SP) :: summ
  n=assert_eq(SIZE(a,1),SIZE(a,2),SIZE(p),'choldc')
  DO i=1,n
     summ=a(i,i)-DOT_PRODUCT(a(i,1:i-1),a(i,1:i-1))
     IF (summ <= 0.0) THEN
        p = 0
        RETURN
     END IF
     p(i)=SQRT(summ)
     a(i+1:n,i)=(a(i,i+1:n)-MATMUL(a(i+1:n,1:i-1),a(i,1:i-1)))/p(i)
  END DO
END SUBROUTINE choldc
