SUBROUTINE ludcmp(a,indx,d)
  USE nrtype; USE nrutil, ONLY : assert_eq,imaxloc,nrerror,outerprod,swap
  IMPLICIT NONE
  REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: a
  INTEGER(I4B), DIMENSION(:), INTENT(OUT) :: indx
  REAL(SP), INTENT(OUT) :: d
  REAL(SP), DIMENSION(SIZE(a,1)) :: vv
  REAL(SP), PARAMETER :: TINY=1.0e-20_sp
  INTEGER(I4B) :: j,n,imax
  n=assert_eq(SIZE(a,1),SIZE(a,2),SIZE(indx),'ludcmp')
  d=1.0
  vv=MAXVAL(ABS(a),dim=2)
  IF (ANY(vv == 0.0)) CALL nrerror('singular matrix in ludcmp')
  vv=1.0_sp/vv
  DO j=1,n
     imax=(j-1)+imaxloc(vv(j:n)*ABS(a(j:n,j)))
     IF (j /= imax) THEN
        CALL swap(a(imax,:),a(j,:))
        d=-d
        vv(imax)=vv(j)
     END IF
     indx(j)=imax
     IF (a(j,j) == 0.0) a(j,j)=TINY
     a(j+1:n,j)=a(j+1:n,j)/a(j,j)
     a(j+1:n,j+1:n)=a(j+1:n,j+1:n)-outerprod(a(j+1:n,j),a(j,j+1:n))
  END DO
END SUBROUTINE ludcmp
