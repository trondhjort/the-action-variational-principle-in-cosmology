SUBROUTINE tred2(a,d,e,novectors)
  USE nrtype; USE nrutil, ONLY : assert_eq,outerprod
  IMPLICIT NONE
  REAL, DIMENSION(:,:), INTENT(INOUT) :: a
  REAL, DIMENSION(:), INTENT(OUT) :: d,e
  LOGICAL, OPTIONAL, INTENT(IN) :: novectors
  INTEGER :: i,j,l,n
  REAL :: f,g,h,hh,scale
  REAL, DIMENSION(SIZE(a,1)) :: gg
  LOGICAL, SAVE :: yesvec=.TRUE.
  n=assert_eq(SIZE(a,1),SIZE(a,2),SIZE(d),SIZE(e),'tred2')
  IF (PRESENT(novectors)) yesvec=.NOT. novectors
  DO i=n,2,-1
     l=i-1
     h=0.0
     IF (l > 1) THEN
        scale=SUM(ABS(a(i,1:l)))
        IF (scale == 0.0) THEN
           e(i)=a(i,l)
        ELSE
           a(i,1:l)=a(i,1:l)/scale
           h=SUM(a(i,1:l)**2)
           f=a(i,l)
           g=-SIGN(SQRT(h),f)
           e(i)=scale*g
           h=h-f*g
           a(i,l)=f-g
           IF (yesvec) a(1:l,i)=a(i,1:l)/h
           DO j=1,l
              e(j)=(DOT_PRODUCT(a(j,1:j),a(i,1:j)) &
                   +DOT_PRODUCT(a(j+1:l,j),a(i,j+1:l)))/h
           END DO
           f=DOT_PRODUCT(e(1:l),a(i,1:l))
           hh=f/(h+h)
           e(1:l)=e(1:l)-hh*a(i,1:l)
           DO j=1,l
              a(j,1:j)=a(j,1:j)-a(i,j)*e(1:j)-e(j)*a(i,1:j)
           END DO
        END IF
     ELSE
        e(i)=a(i,l)
     END IF
     d(i)=h
  END DO
  IF (yesvec) d(1)=0.0
  e(1)=0.0
  DO i=1,n
     IF (yesvec) THEN
        l=i-1
        IF (d(i) /= 0.0) THEN
           gg(1:l)=MATMUL(a(i,1:l),a(1:l,1:l))
           a(1:l,1:l)=a(1:l,1:l)-outerprod(a(1:l,i),gg(1:l))
        END IF
        d(i)=a(i,i)
        a(i,i)=1.0
        a(i,1:l)=0.0
        a(1:l,i)=0.0
     ELSE
        d(i)=a(i,i)
     END IF
  END DO
END SUBROUTINE tred2
