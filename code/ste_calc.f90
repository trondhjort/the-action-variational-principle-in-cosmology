!     Subroutine for calculating the coefficients C_{i,n} from a given 
!     startvalue using the Method of Steepest Descent.
!     ----------------------------------------------------------------
!
SUBROUTINE calc
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : action,gradient,hessian,secant
  IMPLICIT NONE
  REAL :: act,actold,lmda,tmp1
  REAL, PARAMETER :: tol=1
  REAL, DIMENSION(dim) :: dir,tmp2
!
! Initializing:
!
  grad = gradient(coeff)
  actold = action(coeff)
  dir = -grad
  lmda = 10.0_dp
!
! Starting the iteration:
!
  DO
     it = it + 1
!
! Calculating the steplength, coeffecients and gradient:
!
     tmp2 = coeff + lmda*dir
     act = action(tmp2)
     DO WHILE (actold <= act)
        IF (lmda < tol) THEN
           lmda = 10.0
           tmp2 = coeff + lmda*dir
           act = action(tmp2)
           EXIT
        END IF
        lmda = lmda*0.8_dp
        tmp2 = coeff + lmda*dir
        act = action(tmp2)
     END DO 
     actold = act
     coeff = tmp2 
!!$     coeff = secant(coeff,dir,lmda)
     grad = gradient(coeff)
     normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
! Output:
!
!!$     tmp1 = MODULO(it,100)
!!$     IF (tmp1 == 0) THEN
!!$        PRINT '(22x,I7,7x,E12.4)',it,normgrad
!!$     END IF
!
! Termination test:
!
     IF (normgrad < eps) EXIT
!
! Calculating new direction:
!
     dir = -grad
!
  END DO
!
  DEALLOCATE(grad)
  ALLOCATE (hess(dim,dim))
  hess = hessian(coeff)
!
END SUBROUTINE calc


