!     Subroutine for calculating the coefficients C_{i,n} from a given
!     startvalue using the Newton-Raphson Method.
!     ----------------------------------------------------------------
!
SUBROUTINE calc
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : action,gradient,hessian,lnsrch,lubksb,ludcmp,&
                       secant,choldc,cholsl
  IMPLICIT NONE
  INTEGER, DIMENSION(dim) :: indx
  REAL :: act,actold,lmda,normdir,oe,stpmax,tmp
  REAL, DIMENSION(dim) :: dir,coeffold,diag
  REAL, DIMENSION(dim,dim) :: mat
  LOGICAL :: check
  ALLOCATE (hess(dim,dim))
!
! Initializing:
!
  stpmax = MAX(SQRT(DOT_PRODUCT(coeff,coeff)),REAL(dim))
  act = action(coeff)
!
! Starting the iteration:
!
  DO
     it = it + 1
!
! Calculating the gradient:
!
     grad = gradient(coeff)
     normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
! Output:
!
!     tmp = MODULO(it,10)
!     IF (tmp == 0) THEN
!        PRINT '(22x,I7,7x,E12.4)',it,normgrad
!     END IF
!
! Termination test:
!
     IF (normgrad < eps) EXIT
!
! Calculating new direction, steplength, and coeffecients:
!
     dir = -grad
     hess = hessian(coeff)
     mat = hess
     CALL choldc(mat,diag)
     IF (ALL(diag==0)) THEN          ! i.e. non-positive definite
        CALL ludcmp(hess,indx,oe)
        CALL lubksb(hess,indx,dir)
     ELSE
        CALL cholsl(mat,diag,dir,dir)
     END IF
     actold = act
     coeffold = coeff
     CALL lnsrch(coeffold,actold,grad,dir,coeff,act,stpmax,check,action)
!!$     normdir = SQRT(DOT_PRODUCT(dir,dir))
!!$     IF (normdir > 10.0) THEN
!!$        lmda = 0.3_dp
!!$     ELSE
!!$        lmda = 1.0_dp
!!$     END IF
!!$     coeff = coeff + lmda*dir
!!$     coeff = secant(coeffold,dir,lmda)
!
  END DO
!
  hess = hessian(coeff)
  DEALLOCATE(grad)
!
END SUBROUTINE calc

