SUBROUTINE calc
!
!     Subroutine for calculating the coefficients C_{i,n} from a given
!     startvalue using the Secant method (DFP & BFGS updating formula).
!     -----------------------------------------------------------------
!
  USE nrtype; USE avp_data
  USE avp_intf, ONLY : action,hessian,gradient,lnsrch,secant
  USE nrutil, ONLY : outerprod,unit_matrix
  IMPLICIT NONE
  INTEGER, DIMENSION(dim) :: indx
  REAL :: act,actold,lmda,sumy,sumd,stpmax,tmp1,yd,yhy
  REAL, PARAMETER :: tol=EPSILON(coeff)
  REAL, DIMENSION(dim) :: dir,hy,coeffold,y,dhy
  LOGICAL :: check
  ALLOCATE (hessin(dim,dim))
!
! Initializing some variables:
!
  stpmax = MAX(SQRT(DOT_PRODUCT(coeff,coeff)),REAL(dim))
  act = action(coeff)
  grad = gradient(coeff)
  CALL unit_matrix(hessin)        ! from Press et al. (1996)
  hessin = ABS(act)*hessin
  dir = -grad     
!
! Starting the iterations:
!
  DO
     it = it + 1
!
!    Calculating the steplength, coeffecients and gradient:
!
     actold = act
     coeffold = coeff
!!$     CALL lnsrch(coeffold,actold,grad,dir,coeff,act,stpmax,check,action)
!                                 ! from Press et al. (1996)
     coeff = secant(coeffold,dir,lmda)
     dir = coeff - coeffold
     y = grad
     grad = gradient(coeff)
     normgrad = SQRT(DOT_PRODUCT(grad,grad))
!
! Output:
!
!!$     tmp1 = MODULO(it,100)
!!$     IF (tmp1 == 0) THEN
!!$        PRINT '(22x,I7,7x,E12.4)',it,normgrad
!!$     END IF
!
! Termination test:
!
     IF (normgrad < eps) EXIT
!
! Calculating the new direction:
!
     y = grad - y
     yd = DOT_PRODUCT(y,dir)
     sumy = DOT_PRODUCT(y,y)
     sumd = DOT_PRODUCT(dir,dir)
!
!    Skip update of inverse Hesssian if not sufficiently positive:
!
     IF (yd**2 > tol*sumy*sumd) THEN
        hy = MATMUL(hessin,y)
        yhy = DOT_PRODUCT(y,hy)
        y = dir/yd - hy/yhy
        hessin = hessin + outerprod(dir,dir)/yd - &
             outerprod(hy,hy)/yhy + yhy*outerprod(y,y)
     END IF
     dir = -MATMUL(hessin,grad)
!
  END DO
!
  DEALLOCATE(grad,hessin)
  ALLOCATE (hess(dim,dim))
  hess = hessian(coeff)
!
END SUBROUTINE calc
